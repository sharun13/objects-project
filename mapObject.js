function mapObject(testObject, callBack) {
    if (testObject == undefined) {
        return [];
    } 
    if (!(typeof(testObject) == 'object')) {
        return [];
    } else {
        for (let key in testObject) {
            testObject[key] = (callBack(testObject[key]));
        }
        return testObject;
    }
    return [];
}

module.exports = mapObject;