function pairs(testObject) {
    if (testObject == undefined) {
        return [];
    } 
    if (!(typeof(testObject) == 'object')) {
        return [];
    } else {
        let invertObject = {};
        for (let key in testObject) {
            let temporaryKey;
            let temporaryValue;
            temporaryKey = key;
            temporaryValue = testObject[key];
            invertObject[temporaryValue] = temporaryKey;
        }
        return invertObject;
    }
    return [];
}

module.exports = pairs;