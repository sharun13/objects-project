function keys(testObject) {
    if (testObject == undefined) {
        return [];
    } 
    if (!(typeof(testObject) == 'object')) {
        return [];
    } else {
        let keyString = [];
        for (let key in testObject) {
            keyString.push(key);
        }
        return keyString;
    }
    return [];
}

module.exports = keys;