let testObject = require('../testObject');
let mapObject = require('../mapObject');

function callBack(value) {
    return (value + " Test");
}

const mapResult = mapObject(testObject, callBack);
console.log(mapResult);