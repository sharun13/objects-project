let testObject = require('../testObject');
let defaults = require('../defaults');
let defaultProperties = { name: 'Bruce Wayne', age: 36, location: 'Gotham', alias: 'batman', isRich: true, owns: 'Wayne Corp' };

const result = defaults(testObject, defaultProperties);

console.log(result);