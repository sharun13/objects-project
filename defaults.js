function defaults(testObject, defaultProperties) {
    if (testObject == undefined) {
        return [];
    } 
    if (!(typeof(testObject) == 'object')) {
        return [];
    } else {
        for (let key in defaultProperties) {
            if (key in testObject) {
                continue;
            } else {
                testObject[key] = defaultProperties[key];
            }
        }
        return testObject;
    }
    return [];
}

module.exports = defaults;