function pairs(testObject) {
    if (testObject == undefined) {
        return [];
    } 
    if (!(typeof(testObject) == 'object')) {
        return [];
    } else {
        let pairsObject = [];
        for (let key in testObject) {
            let temporaryVariable;
            let temporaryObject = [];
            temporaryVariable = key;
            temporaryObject.push(temporaryVariable);
            temporaryVariable = testObject[key];
            temporaryObject.push(temporaryVariable);
            pairsObject.push(temporaryObject);
        }
        return pairsObject;
    }
    return [];
}

module.exports = pairs;