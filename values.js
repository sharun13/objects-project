function values(testObject) {
    if (testObject == undefined) {
        return [];
    } 
    if (!(typeof(testObject) == 'object')) {
        return [];
    } else {
        let valueString = [];
        for (let key in testObject) {
            valueString.push(testObject[key]);
        }
        return valueString;
    }
    return [];
}

module.exports = values;